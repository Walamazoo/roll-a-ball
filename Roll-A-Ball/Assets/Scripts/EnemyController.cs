﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 0;
    public GameObject playerObject;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Moves the enemy towards the player
    //Made with the help of:https://answers.unity.com/questions/616195/how-to-make-an-object-go-the-direction-it-is-facin.html
    void FixedUpdate()
    {
        transform.LookAt(playerObject.transform);
        transform.position += transform.forward * Time.deltaTime * speed;
    }
}
