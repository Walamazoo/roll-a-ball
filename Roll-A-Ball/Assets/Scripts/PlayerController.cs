﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

//Code for timer created with help from https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public float timeRemaining = 45;
    public bool timerIsRunning = false; 
    public TextMeshProUGUI countText;
    public TextMeshProUGUI timerText;
    public GameObject winTextObject;
    public GameObject loseTextObject;
    public GameObject player;
    public GameObject enemy;
    public GameObject enemy1;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        timerText.text = "Time: " + timeRemaining.ToString();
        winTextObject.SetActive(false);
        loseTextObject.SetActive(false);
        timerIsRunning = true;
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 12)
        {
            winTextObject.SetActive(true);
            enemy.SetActive(false);
            enemy1.SetActive(false);
            timerIsRunning = false;
        }
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement*speed);
        if(timerIsRunning){
            if(timeRemaining > 0){
                timeRemaining -= Time.deltaTime;
                timerText.text = "Time: " + timeRemaining.ToString();
            }
            else{
                timeRemaining = 0;
                timerText.text = "Time: " + timeRemaining.ToString();
                timerIsRunning = false;
                loseTextObject.SetActive(true);
                player.SetActive(false);
             }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
           other.gameObject.SetActive(false); 
           count = count + 1;
           SetCountText();
        }
        else if(other.gameObject.CompareTag("Enemy"))
        {
            loseTextObject.SetActive(true);
            player.SetActive(false);
        }
    }
}
